# Docker-in-Docker Integration Image Builder

This image extends the Docker-maintained `stable-git` image to have the following additional installed dependencies for use as an integration image for Docker-based projects in CI pipelines:

```
docker-compose
make
```

To use it in a pipeline in Gitlab CI, login to docker and copy the contents of your `$HOME/.docker/config.json` into the `DOCKER_AUTH_CONFIG` variable in Gitlab, then start your `.gitlab-ci.yml` with something to the effect of:

```
image: <registry>/<group>/<project>:<tag>

services:
  - docker:dind

before_script:
  - mkdir $HOME/.docker
  - echo $DOCKER_AUTH_CONFIG > $HOME/.docker/config.json
```
