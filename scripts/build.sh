#!/usr/bin/sh
. $(dirname $0)/env.sh

# verbose command output
set -ux

# pull latest for caching
docker pull $CONTAINER_IMAGE_NAME

# exit on error
set -e

# try to pull the tag then build regardless
docker build --pull -t "$CONTAINER_IMAGE_NAME:$COMMIT_SHORT_SHA" .

# push the tag no matter what
docker push $CONTAINER_IMAGE_NAME:$COMMIT_SHORT_SHA
