#!/usr/bin/sh
. $(dirname $0)/env.sh

# verbose command output and early exit
set -eux

# re-tag latest images with this commit slug
docker pull $CONTAINER_IMAGE_NAME:$COMMIT_SHORT_SHA

# tag latest for master or tag
docker tag $CONTAINER_IMAGE_NAME:$COMMIT_SHORT_SHA $CONTAINER_IMAGE_NAME

# tag with tag only for tag then push
if [ $COMMIT_TAG ]; then
    docker tag $CONTAINER_IMAGE_NAME:$COMMIT_SHORT_SHA $CONTAINER_IMAGE_NAME:$COMMIT_TAG
    docker push $CONTAINER_IMAGE_NAME:$COMMIT_TAG
fi

# finally, push latest
docker push $CONTAINER_IMAGE_NAME
